# 프로젝트 설명

 - 안드로이드 코드: /MyApplications/

 - 클라이언트 html 코드: /html/ (업로드 된 주소: http://webrtc.games.naim.kewtea.com/html)

 - peer.js 중계 서버 주소: https://webrtc.games.naim.kewtea.com

 - sockiet.io 중계 서버 주소: https://socket.kewtea.com

 - /app-rtc/ directory는, Android native + sockiet.io WebRTC 예제 코드 입니다.

email: sangsoo.lee@kewtea.com
